//
//  SettingsStartdayView.swift
//  1MinuteWorkouts
//
//  Created by Justin Mandell on 10/30/20.
//

import SwiftUI

struct SettingsStartdayView: View {
    
    @AppStorage("isOn_StartDayNotification") var isOn_StartDayNotification: Bool = true
    @State private var firstNotificationTime = Date()
    //@AppStorage("firstNotificatoinTimeSetting") var firstNotificatoinTimeSetting: Date = firstNotificationTime
    @State private var isMonOn = true
    @State private var isTueOn = true
    @State private var isWedOn = true
    @State private var isThuOn = true
    @State private var isFriOn = true
    @State private var isSatOn = false
    @State private var isSunOn = false
    
    var body: some View {
        VStack {
            Text("Set your first workout reminder to an hour after you usually start your work day.")
                .fontWeight(.semibold)
                .padding()
                .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color(hue: 0.513, saturation: 0.159, brightness: 0.988)/*@END_MENU_TOKEN@*/)
                .cornerRadius(8)
                .padding()
            List{
                HStack {
                    Toggle(isOn: $isOn_StartDayNotification) {
                        Text("Start Day Notification")
                    }
                }
                DatePicker("First Workout Notification Time", selection: $firstNotificationTime, displayedComponents: .hourAndMinute)
                
                //------------ Days of the week to have a Start Day notificaion sent ------------//
                HStack {
                    Text("Monday")
                    Spacer()
                    Image(systemName: isMonOn ? "checkmark.circle.fill" : "checkmark.circle")
                        .foregroundColor(isMonOn ? .blue : /*@START_MENU_TOKEN@*/.gray/*@END_MENU_TOKEN@*/)
                }
                .onTapGesture {
                    self.isMonOn.toggle()
                }
                //.hidden()
                
                HStack {
                    Text("Tuesday")
                    Spacer()
                    Image(systemName: isTueOn ? "checkmark.circle.fill" : "checkmark.circle")
                        .foregroundColor(isTueOn ? .blue : /*@START_MENU_TOKEN@*/.gray/*@END_MENU_TOKEN@*/)
                }
                .onTapGesture {
                    self.isTueOn.toggle()
                }
                
                HStack {
                    Text("Wednesday")
                    Spacer()
                    Image(systemName: isWedOn ? "checkmark.circle.fill" : "checkmark.circle")
                        .foregroundColor(isWedOn ? .blue : /*@START_MENU_TOKEN@*/.gray/*@END_MENU_TOKEN@*/)
                }
                .onTapGesture {
                    self.isWedOn.toggle()
                }
                
                HStack {
                    Text("Thursday")
                    Spacer()
                    Image(systemName: isThuOn ? "checkmark.circle.fill" : "checkmark.circle")
                        .foregroundColor(isThuOn ? .blue : /*@START_MENU_TOKEN@*/.gray/*@END_MENU_TOKEN@*/)
                }
                .onTapGesture {
                    self.isThuOn.toggle()
                }
                
                HStack {
                    Text("Friday")
                    Spacer()
                    Image(systemName: isFriOn ? "checkmark.circle.fill" : "checkmark.circle")
                        .foregroundColor(isFriOn ? .blue : /*@START_MENU_TOKEN@*/.gray/*@END_MENU_TOKEN@*/)
                }
                .onTapGesture {
                    self.isFriOn.toggle()
                }
                
                HStack {
                    Text("Saturday")
                    Spacer()
                    Image(systemName: isSatOn ? "checkmark.circle.fill" : "checkmark.circle")
                        .foregroundColor(isSatOn ? .blue : /*@START_MENU_TOKEN@*/.gray/*@END_MENU_TOKEN@*/)
                }
                .onTapGesture {
                    self.isSatOn.toggle()
                }
                
                HStack {
                    Text("Sunday")
                    Spacer()
                    Image(systemName: isSunOn ? "checkmark.circle.fill" : "checkmark.circle")
                        .foregroundColor(isSunOn ? .blue : /*@START_MENU_TOKEN@*/.gray/*@END_MENU_TOKEN@*/)
                }
                .onTapGesture {
                    self.isSunOn.toggle()
                }
                
            }
        }
        .navigationBarTitle("Start Day Notification", displayMode: .inline)
        .onAppear(){
            print("current time on picker \(firstNotificationTime)")
        }
    }
}

struct SettingsStartdayView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsStartdayView()
            .previewDevice("iPhone 12 mini")
    }
}
