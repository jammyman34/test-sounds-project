//
//  SettingsHomeView.swift
//  1MinuteWorkouts
//
//  Created by Justin Mandell on 10/30/20.
//

import SwiftUI

struct SettingsHomeView: View {
    
    @State var startDayNotificationSetting: String = "8:30AM"
    @State var appVersion: String = "0.01"
    
    var body: some View {
//        NavigationView{
            //Settings Home page will go here
            VStack {
                HStack {
                    Text("Settings")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        .multilineTextAlignment(.leading)
                        .padding(.leading)
                    Spacer()
                }.padding(.top, -64) .padding(.bottom, -64)
                List {
                    NavigationLink(destination: SettingsStartdayView()){
                        HStack {
                            Text("Start Day Notification")
                            Spacer()
                            Text(startDayNotificationSetting)
                                .font(.subheadline)
                                .foregroundColor(Color.gray)
                                .multilineTextAlignment(.trailing)
                            //Image(systemName: "chevron.right")
                        }
                    }
                }
            }
    }
}

struct SettingsHomeView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsHomeView()
            .previewDevice("iPhone 12 mini")
    }
}
