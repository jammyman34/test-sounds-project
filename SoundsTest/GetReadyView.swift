//
//  GetReadyView.swift
//  SoundsTest
//
//  Created by Justin Mandell on 2/8/21.
//

import SwiftUI
import AVFoundation

struct GetReadyCountdownView: View {
    
    @State var interstitialTitle: String = "Get Ready!"
    @State private var getReadyCountdownTime = 5
    @Environment(\.presentationMode) var presentationMode
    //@State private var isPlaying : Bool = false
    @Binding var isShowing: Bool
    
    var body: some View {
        ZStack {
            Rectangle()
                //.fill(Color(red: 0.0, green: 0.0, blue: 0.0, opacity: 0.4))
                .fill(Color.white)
                //.edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                .frame(width: .infinity, height: 230, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .cornerRadius(8)
                .shadow(color: .gray, radius: 5, x: /*@START_MENU_TOKEN@*/-2.0/*@END_MENU_TOKEN@*/, y: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/)
            
            VStack {
                Text(interstitialTitle)
                    .font(.title)
                    .fontWeight(.semibold)
                    .multilineTextAlignment(.center)
                    .foregroundColor(Color("Charcoal"))
                    .padding(.top, 8)
                
                Text("\(Int(getReadyCountdownTime))")
                    .font(.system(size: 70))
                    .fontWeight(.semibold)
                    .foregroundColor(Color("Charcoal"))
                    .onAppear() {
                        //getReady() //plays get ready sound
                        Sounds.playSounds(soundfile: "GetRead.m4a")
                        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
                            self.getReadyCountdownTime -= 1
                            print(self.getReadyCountdownTime)
                            if self.getReadyCountdownTime == 0 {
                                timer.invalidate()
                                //self.presentationMode.wrappedValue.dismiss() //dismisses sheet
                                self.isShowing = false
                                print("done")
                            }
                        }
                        
                    }
            }
//            .background(Color.white)
//            .cornerRadius(8)
        }
    }
}

func getReady(){
    //AudioServicesPlaySystemSound(1026)
    //AudioServicesPlayAlertSound(1026)
    Sounds.playSounds(soundfile: "GetReady.m4a")
}

//struct GetReadyCountdownView_Previews: PreviewProvider {
//    static var previews: some View {
//        GetReadyCountdownView()
//            .previewDevice("iPhone 12 mini")
//    }
//}
