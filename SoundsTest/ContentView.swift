//
//  ContentView.swift
//  SoundsTest
//
//  Created by Justin Mandell on 2/8/21.
//

import SwiftUI
import AVFoundation

struct ContentView: View {
    
    @State private var isSelectedTab = 0
    
    var body: some View {
        NavigationView{
            TabView(selection: $isSelectedTab) {
                VStack {
                    Button(action:{
                        //actions go here
                        //plays get ready sound
                        Sounds.playSounds(soundfile: "Get Ready.m4a")
                    }, label:{
                        Text("Get Ready")
                            .fontWeight(.semibold)
                    }).padding()
                    
                    Button(action:{
                        //actions go here
                        //plays get ready sound
                        Sounds.playSounds(soundfile: "Begin.m4a")
                    }, label:{
                        Text("Begin")
                            .fontWeight(.semibold)
                    }).padding()
                    
                    Button(action:{
                        //actions go here
                        //plays get ready sound
                        Sounds.playSounds(soundfile: "10 Seconds.m4a")
                    }, label:{
                        Text("10 Seconds")
                            .fontWeight(.semibold)
                    }).padding()
                }
                
                .tabItem {
                    Image(systemName: "hifispeaker")
                    Text("Sounds")
                }.tag(1)
                
                SettingsHomeView()
                    .tabItem {
                        Image(systemName: "gear")
                        Text("Settings")
                    }.tag(2)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
