//
//  SoundsTestApp.swift
//  SoundsTest
//
//  Created by Justin Mandell on 2/8/21.
//

import SwiftUI

@main
struct SoundsTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
